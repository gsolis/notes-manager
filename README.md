# Notes Manager

## UX/Design
`https://app.moqups.com/german.gsl89@gmail.com/bDJWD7luuz/view`

## UI Views

* CREATE notes
* LIST notes
* VIEW single note
* EDIT/UPDATE note
* DELETE note

## Backend API

* GET /notes
* GET /notes/:id
* POST /notes
* PUT /notes/:id
* DELETE /notes/:id

## Note Model
`{
    id: uuid,
    title: string,
    description: string,
    status: string,
    createdDate: date
}`