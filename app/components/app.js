import React from 'react';
import './app.scss';

class App extends React.Component {

    render() {
        return (
            <div className="app">
                <p>Notes manager</p>
                <p>Have fun! <i className="fa fa-smile-o" aria-hidden="true"></i></p>
                <a href="http://fontawesome.io/icons/" >Icons available here!</a>
            </div>
        );
    }

}

export default App;
