import React from 'react';
import ReactDOM from 'react-dom';

import './lib/font-awesome/scss/font-awesome.scss';
import App from './components/app';

ReactDOM.render(
    <App />,
    document.querySelector('#main-app')
);